FROM jboss/keycloak:3.4.3.Final
LABEL maintainer="lasse.lidegaard@alexandra.dk"

ADD --chown=1000:1000 src /opt/jboss/keycloak/themes
